import React, { useEffect, useState } from 'react';

function HatsList() {
    const [hats, setHats] = useState([]);
    const [locationDetails, setLocationDetails] = useState({});

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    };

    const getLocationDetails = async (locationId) => {
        const response = await fetch(`http://localhost:8100/api/locations/${locationId}`);

        if (response.ok) {
            const locationData = await response.json();
            setLocationDetails((prevLocationDetails) => ({
                ...prevLocationDetails,
                [locationId]: locationData,
            }));
        }
    };

    const handleDeleteHat = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (response.ok) {
            // Hat deleted successfully, update the hats state to remove the deleted hat.
            setHats((prevHats) => prevHats.filter((hat) => hat.id !== hatId));
        } else {
            // Handle error here (e.g., show an error message)
            console.error('Failed to delete hat.');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    useEffect(() => {
        hats.forEach((hat) => {
            const locationId = hat.location.id; // Capture unique id for each hat
            if (!locationDetails[locationId]) {
                getLocationDetails(locationId); // Pass id explicitly
            }
        });
    }, [hats, locationDetails]);

    return (
        <table className="table">
            <thead className="table-dark">
                <tr>
                    <th>ID</th>
                    <th>Fabric</th>
                    <th>Style name</th>
                    <th>Color</th>
                    <th>Picture link</th>
                    <th>Location</th>
                    <th>Delete a Hat</th>
                </tr>
            </thead>
            <tbody>
                {hats.map((hat, index) => (
                    <tr key={index}>
                        <td>{hat.id}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.style_name}</td>
                        <td>{hat.color}</td>
                        <td>{hat.picture_url}</td>
                        <td>
                            {locationDetails[hat.location.id] ? (
                                <>
                                    <div>Closet Name: {locationDetails[hat.location.id].closet_name}</div>
                                    <div>Section Number: {locationDetails[hat.location.id].section_number}</div>
                                    <div>Shelf Number: {locationDetails[hat.location.id].shelf_number}</div>
                                </>
                            ) : (
                                'N/A'
                            )}
                        </td>
                        <td>
                            <button
                                onClick={() => handleDeleteHat(hat.id)}
                                className="btn btn-danger"
                            >
                                Delete
                            </button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default HatsList;
