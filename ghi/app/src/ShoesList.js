import React, {useEffect, useState} from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const [binDetails, setBinDetails] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data.shoes)
        }
    };

    const getBinDetails = async (binID) => {
        const response = await fetch(`http://localhost:8100/api/bins/${binID}`);
    
        if (response.ok) {
            const binData = await response.json();
            console.log(binData);
            setBinDetails((prevBinDetails) => ({
                ...prevBinDetails,
                [binID]: binData,
            }));
        }
    };

    const handleDeleteShoes = async (shoeId) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (response.ok) {
            setShoes((prevShoes) => prevShoes.filter((shoe) => shoe.id !== shoeId));
        } else {
            console.error('Failed to delete this set of shoes.');
        }
    };


const fetchBinDetailsForShoes = (binID) => {
    getBinDetails(binID);
};

useEffect(() => {
    getData();
}, []);

useEffect(() => {
    shoes.forEach((shoe) => {
        const binID = shoe.bin.id;
        if (!binDetails[binID]) {
            getBinDetails(binID);
        }
    });
}, [shoes, binDetails]);

    return (
        <table className='table'>
            <thead className='table-dark'>
                <tr>
                    <th>ID</th>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Image</th>
                    <th>Bin</th>
                    <th>Delete Me</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe, index) => (
                    <tr key={index}>
                        <td>{shoe.id}</td>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.name}</td>
                        <td>{shoe.color}</td>
                        <td>{shoe.picture_url}</td>
                        <td>
                            {binDetails[shoe.bin.id] ? (
                                <>
                                    <div>Closet Name: {binDetails[shoe.bin.id].closetname}</div>
                                    <div>Bin Number: {binDetails[shoe.bin.id].bin_number}</div>
                                    <div>Bin Size: {binDetails[shoe.bin.id].bin_size}</div>
                                </>
                            ) : (
                                'N/A'
                            )}
                        </td>
                        <td>
                            <button
                                onClick={() => handleDeleteShoes(shoe.id)}
                                className="btn btn-danger"
                            >
                                Delete
                            </button>

                        </td>
                    </tr>
                    ))}
            </tbody>
        </table>
    );
}

export default ShoesList;