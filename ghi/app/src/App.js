import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
          <Route index element={<HatsList />} />
          <Route path="/hats/new" element={<HatForm />} /></Route>
          <Route path="shoes">
          <Route index element={<ShoesList />} />
          <Route path="/shoes/new" element={<ShoesForm />}></Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
