from django.contrib import admin
from .models import Shoes, BinVO


@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    pass